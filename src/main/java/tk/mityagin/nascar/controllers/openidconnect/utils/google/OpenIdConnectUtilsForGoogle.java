package tk.mityagin.nascar.controllers.openidconnect.utils.google;

import com.google.gson.*;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.springframework.stereotype.Service;
import tk.mityagin.nascar.controllers.openidconnect.utils.OpenIdConnectUtilsImpl;

import java.util.*;

/**
 * Created by Nikita on 11/29/2016.
 */
@Service
public class OpenIdConnectUtilsForGoogle extends OpenIdConnectUtilsImpl {

    public static final String CLIENT_ID = "810685779485-1j39fs1nbt8del87r89h8gbqrnmfkvhc.apps.googleusercontent.com";
    public static final String CLIENT_SECRET = "cCT2m6Ig4Jd3uFtrLTH0sJPq";
    public static final String AUTHENTICATION_URL = "https://accounts.google.com/o/oauth2/v2/auth";
    public static final String TOKEN_URL = "https://www.googleapis.com/oauth2/v4/token";

    /** There are no mandatory claims, they are choose of OpenIdConnect provider.
     *  So specific for google:
     */
    public static final String[] CLAIMS = {
            "iss", "sub", "aud", "iat", "exp", "at_hash", "email_verified",
            "azp", "email", "profile", "picture", "name", "hd"
    };

    private List<String> certificates = new ArrayList<>(1);

    /** TODO: JWT validation
     *  1. Verify that the ID token is properly signed by the issuer. Google-issued tokens are signed using one of the
     *     certificates found at the URI specified in the jwks_uri field of the discovery document.
     *     (https://tools.ietf.org/html/rfc7517#section-5)
     *  2. Verify that the value of iss in the ID token is equal to https://accounts.google.com or accounts.google.com.
     *  3. Verify that the value of aud in the ID token is equal to your app’s client ID.
     *  4. Verify that the expiry time (exp) of the ID token has not passed.
     *  5. If you passed a hd parameter in the request, verify that the ID token has a hd claim that matches your G Suite
     *     hosted domain.
     * @param jwt
     */
    @Override
    protected boolean isJwtValid(JsonObject jwt) {
        //fillCertificates();

        // TODO: implement - see description above

        return true;
    }

    private void fillCertificates() throws UnirestException {
        certificates.clear();

        String responseBody = Unirest.get("https://www.googleapis.com/oauth2/v3/certs").asString().getBody();
        JsonObject certificatesObject = new JsonParser().parse(responseBody).getAsJsonObject();
        JsonArray keys = certificatesObject.get("keys").getAsJsonArray();

        for (JsonElement key: keys) {
            certificates.add(key.getAsJsonObject().get("n").getAsString());
        }
    }

    @Override
    protected boolean isClaimAvailable(String claim) {
        for (String specificClaim: CLAIMS) {
            if (specificClaim.equals(claim)) {
                return true;
            }
        }

        return false;
    }
}
