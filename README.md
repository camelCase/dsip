# DSIP Laboratory work 1 #

* Openid Connect client for google services
* Available on [heroku](https://fast-oasis-94644.herokuapp.com/signin)

### Workflow: ###

* Go to heroku and click Sign In
* If there is a first access then you will be redirected to google form to provide access (to your personal data and read only gmail) for this app
* * If permissions were provided erlier through the google form then there wil be immediate redirect to list of threads
* * To take away the permissions it is needed to remove 'Tasks' application [here](https://security.google.com/settings/security/permissions)
* Then there will be shown list of last threads from your Inbox (the data is not stored in this application)

**Note that there is no security guaranties for your credentials :( (For example, there is no CSRF protection etc)**

### Description: ###
OpenidConnect logic is implemented in OpenIdConnectUtilsImpl (Interface OpenIdConnectUtils). It is used in OpenIDConnectController to compose request for 'code', request for tokens and public APIs of google (As example used Gmail API)